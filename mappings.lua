local M = {}

M.loclist = {
    n = {
		["<leader>lo"] = { "<cmd> :lopen <CR>", "open loclist" },
		["<leader>lc"] = { "<cmd> :lclose <CR>", "close loclist" },
		["<leader>ln"] = { "<cmd> :lnext <CR>", "next loclist item" },
		["<leader>lp"] = { "<cmd> :lprev <CR>", "previous loclist item" },
		["<leader>l<n>"] = { "<cmd> :ll<n> <CR>", "nth loclist item" },
    }
}

M.telescope = {

	n = {
		-- find
		["<leader>ff"] = { "<cmd> Telescope find_files <CR>", "find files" },
		["<leader>fa"] = { "<cmd> Telescope find_files follow=true no_ignore=true hidden=true <CR>", "find all" },
		["<leader>fw"] = { "<cmd> Telescope live_grep <CR>", "live grep" },
		["<leader>fB"] = { "<cmd> Telescope buffers <CR>", "find buffers" },
		["<leader>fh"] = { "<cmd> Telescope help_tags <CR>", "help page" },
		["<leader>fo"] = { "<cmd> Telescope oldfiles <CR>", "find oldfiles" },
		["<leader>fz"] = { "<cmd> Telescope current_buffer_fuzzy_find <CR>", "find in current buffer" },
		["<leader>gf"] = { "<cmd> Telescope git_files <CR>", "find git files" },

		-- git
		["<leader>cm"] = { "<cmd> Telescope git_commits <CR>", "git commits" },
		["<leader>gt"] = { "<cmd> Telescope git_status <CR>", "git status" },

		-- pick a hidden term
		["<leader>pt"] = { "<cmd> Telescope terms <CR>", "pick hidden term" },

		-- theme switcher
		["<leader>th"] = { "<cmd> Telescope themes <CR>", "nvchad themes" },

		-- notes
		["<leader>gn"] = { "<cmd> lua require'custom.funcs'.grep_notes() <CR>", "search notes content" },
		["<leader>fn"] = { "<cmd> lua require'custom.funcs'.find_notes() <CR>", "search note files" },

		-- configs
		["<leader>gc"] = { "<cmd> lua require'custom.funcs'.grep_configs() <CR>", "search configs content" },
		["<leader>fc"] = { "<cmd> lua require'custom.funcs'.find_configs() <CR>", "search config files" },

		-- file_browser
		["<leader>fb"] = { "<cmd> Telescope file_browser <CR>", "file browser" },
	},
}

local function fn_harpoon_nav_file(n)
	return function()
		require("harpoon.ui").nav_file(n)
	end
end

M.harpoon = {

	n = {
		["<leader>h"] = {
			function()
				require("harpoon.ui").toggle_quick_menu()
			end,
			"open harpoon quick menu",
		},
		["<leader>a"] = {
			function()
				require("harpoon.mark").add_file()
			end,
			"Add harpoon mark",
		},
		["<leader>1"] = { fn_harpoon_nav_file(1), "open harpoon file 1" },
		["<leader>2"] = { fn_harpoon_nav_file(2), "open harpoon file 2" },
		["<leader>3"] = { fn_harpoon_nav_file(3), "open harpoon file 3" },
		["<leader>4"] = { fn_harpoon_nav_file(4), "open harpoon file 4" },
	},
}

M.undotree = {

	n = {
		["<leader>u"] = { "<cmd> UndotreeToggle <CR>", "toggle undotree" },
	},
}

-- M.tabufline = {
--
-- 	n = {
-- 		},
-- 	},
-- }

M.gopher = {

    n = {
        ["<leader>gsj"] = { -- GoStructJson
          "<cmd> GoTagAdd json <CR>",
          "Add json struct tags"
        },
        ["<leader>gsy"] = { -- GoStructYaml
          "<cmd> GoTagAdd yaml <CR>",
          "Add yaml struct tags"
        },
    },
}

M["zen-mode"] = {

	n = {
		["<leader>zm"] = { "<cmd> ZenMode <CR>", "toggle ZenMode" },
	},
}

return M
