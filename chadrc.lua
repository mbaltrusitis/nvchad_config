---@type ChadrcConfig
local M = {}

M.ui = {
  theme = 'tomorrow_night',
  statusline = {
    theme = "minimal", -- {default,vscode,vscode_colored,minimal}
  },
}

M.plugins = "custom.plugins"
M.mappings = require "custom.mappings"

return M
