local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"
local servers = {
  "bashls",
  "clangd",
  "cssls",
  "dockerls",
  "elixirls",
  "erlangls",
  "gopls",
  "html",
  "jsonls",
  "pyright",
  "rust_analyzer",
  "tailwindcss",
  "tsserver",
}

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

lspconfig.elixirls.setup {
	cmd = {"elixir-ls"},
	on_attach = on_attach,
	capabilities = capabilities,
}
