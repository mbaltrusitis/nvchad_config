local null_ls = require "null-ls"

local formatting = null_ls.builtins.formatting
local lint = null_ls.builtins.diagnostics

local sources = {
  formatting.beautysh,
  formatting.black,
  formatting.erlfmt,
  formatting.gofumpt,
  formatting.goimports_reviser,
  formatting.golines,
  formatting.json_tool,
  formatting.mix,
  formatting.prettier,
  lint.credo,
  lint.mypy,
  lint.ruff,
  lint.shellcheck,
}

null_ls.setup {
  debug = true,
  sources = sources,
}
